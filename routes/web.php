<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/map', function(){
    dd('nice!');
});

Route::post('/saveProgressWig', 'CustomController@saveProgressWig');
Route::post('/saveProgressLead', 'CustomController@saveProgressLead');
Route::get('api/getWig/{id}', 'CustomController@getWig');
Route::get('api/getLead/{id}', 'CustomController@getLead');
