<!-- First you need to extend the CB layout -->
@extends('crudbooster::admin_template')
@section('content')
<!-- Your custom  HTML goes here -->

{{-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"> --}}

<style>
    td.details-control {
    background: url('{{ asset("details_open.png") }}') no-repeat center center;
    cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('{{ asset("details_close.png") }}') no-repeat center center;
    }

    #example { 
    border-collapse: separate; 
    border-spacing: 10px; 
    *border-collapse: expression('separate', cellSpacing = '10px');
    }
</style>

<div id="container" style="width:100%; height:400px;"></div>
<br>
<div class="row">
    <div class="col-md-4" style="padding-left:0">
        <div id="container-polar" class="container"></div>
        <div id="container-speed" class="container"></div>
    </div>
    <div class="col-md-8">
        <div id="container-line" ></div>
    </div>
</div>
<div style="width:100%; height:25px;"></div>
<div class="row">
    <div class="col-md-12">
    <div class="panel">
        <table id="example" class="display" style="width:100%">
            <thead>
                <tr>
                    <th></th>
                    <th>Name</th>
                    <th>Go to</th>
                </tr>
            </thead>
        </table>
    </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<!-- ADD A PAGINATION -->
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<script src="https://code.highcharts.com/modules/solid-gauge.js"></script>

<script src="http://code.highcharts.com/maps/modules/map.js"></script>
{{-- <script src="https://code.highcharts.com/maps/highmaps.js"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/proj4js/2.3.12/proj4-src.js"></script>
<script src="https://code.highcharts.com/maps/modules/exporting.js"></script>
<script src="https://code.highcharts.com/mapdata/countries/id/id-all.js"></script>

<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>

<style>
.container {
	width: 100%;
	float: left;
	height: 200px;
}
.highcharts-yaxis-grid .highcharts-grid-line {
	
}

@media (max-width: 600px) {
	.outer {
		width: 100%;
		height: 400px;
	}
	.container {
		width: 300px;
		float: none;
		margin: 0 auto;
	}

}
</style>



<script>
//MAP
Highcharts.mapChart('container', {
    chart: {
        map: 'countries/id/id-all'
    },

    title: {
        text: 'Strategic Map'
    },

    subtitle: {
        text: 'Source map: <a href="http://code.highcharts.com/mapdata/countries/id/id-all.js">Indonesia</a>'
    },

    mapNavigation: {
        enabled: true,
        buttonOptions: {
            verticalAlign: 'bottom'
        },
        enableMouseWheelZoom: false
    },

    plotOptions: {
        series: {
            dataLabels: {
                enabled: false
            },
            point: {
                events: {
                    click: function () {
                        location.href = '#' + this.name;
                    }
                }
            }
        }
    },

    tooltip: {
        headerFormat: '',
        pointFormat: '<b>{point.name}</b><br>Lat: {point.lat}, Lon: {point.lon}'
    },

    series: [{
            name: 'Basemap',
            borderColor: '#fff',
            nullColor: Highcharts.getOptions().colors[7],
            colors: Highcharts.getOptions().colors[5],
            showInLegend: false,
            },
            {
            type: 'mappoint',
            name: 'Cities',
            color: Highcharts.getOptions().colors[3],
            showInLegend: false,
            data: <?php echo json_encode($unit); ?>
            }]
});
</script>

<script>
//GAUGE
    var gaugeOptions = {

    chart: {
        type: 'solidgauge'
    },

    title: null,

    pane: {
        center: ['50%', '85%'],
        size: '140%',
        startAngle: -90,
        endAngle: 90,
        background: {
            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
            innerRadius: '60%',
            outerRadius: '100%',
            shape: 'arc'
        }
    },

    tooltip: {
        enabled: false
    },

    // the value axis
    yAxis: {
        stops: [
            [0.1, '#DF5353'], // green
            [0.5, '#DDDF0D'], // yellow
            [0.9, '#55BF3B'] // red
        ],
        lineWidth: 0,
        minorTickInterval: null,
        tickAmount: 2,
        title: {
            y: -70
        },
        labels: {
            y: 16
        }
    },

    plotOptions: {
        solidgauge: {
            dataLabels: {
                y: 5,
                borderWidth: 0,
                useHTML: true
            }
        }
    }
};

// The speed gauge
var chartSpeed = Highcharts.chart('container-speed', Highcharts.merge(gaugeOptions, {
    yAxis: {
        min: 0,
        max: 100,
        title: {
            text: '% AVG Pencapaian WIG'
        }
    },

    credits: {
        enabled: false
    },

    exporting: { enabled: false },

    series: [{
        name: 'Speed',
        data: [<?php echo $chart['gauge']?>],
        dataLabels: {
            format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
                   '<span style="font-size:12px;color:silver">%</span></div>'
        },
        tooltip: {
            valueSuffix: ' %'
        }
    }]

}));

</script>

<script>
//LINE CHART
Highcharts.chart('container-line', {

    title: {
        text: 'AVG Progress Pencapaian WIG'
    },

    subtitle: {
        text: ''
    },

    yAxis: {
        title: {
            text: 'Precentage'
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },

    xAxis: {
        type: 'datetime'
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            },
            pointStart: Date.UTC(2018, 0, 1),
            pointInterval: 1,
            pointIntervalUnit: 'month'
        }
    },

    series: <?php echo json_encode($chart['line'])?>,

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

});
</script>

<script>
//POLAR CHART
    Highcharts.chart('container-polar', {

    chart: {
        polar: true
    },

    credits: {
        enabled: false
    },

    title: {
        text: 'WIG Strength'
    },


    xAxis: {
        categories: <?php echo json_encode($chart['polar']['category'])?>,
    },

    yAxis: {
        min: 0
    },

    plotOptions: {
    },

    series: [{
        type: 'area',
        name: 'Area',
        data: [<?php echo join($chart['polar']['polar_data'], ',') ?>]
    }]
});
</script>

<script>
function format ( d ) {
    console.log(d);
    // `d` is the original data object for the row
    return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
        '<tr>'+
            '<td>Full name:</td>'+
            '<td>'+d.name+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Extension number:</td>'+
            '<td>'+d.extn+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Extra info:</td>'+
            '<td>And any further details here (images etc)...</td>'+
        '</tr>'+
    '</table>';
}

$(document).ready(function() {
    var table = $('#example').DataTable( {
        "ajax": "http://localhost/4dx/public/data.txt",
        "columns": [
            {
                "className":      'details-control',
                "orderable":      false,
                "data":           null,
                "defaultContent": ''
            },
            { "data": "name" },
            { "data": "goto" }
        ],
        "bPaginate": false,
        "bLengthChange": false,
        "bFilter": false,
        "bInfo": false,
        "bAutoWidth": false,
        "order": [[1, 'asc']]
    } );
     
    // Add event listener for opening and closing details
    $('#example tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    } );
} );
</script>

@endsection